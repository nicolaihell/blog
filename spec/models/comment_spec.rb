require 'spec_helper'

describe Comment do
	before(:each) do
		@comment = Comment.new(:post_id => 1, 
      :body => "bar")
	end

  	it "is valid with valid attributes" do
    	@comment.should be_valid
  	end
	it "is not valid without a post_id" do
   	 	@comment.post_id = nil
    	@comment.should_not be_valid
  	end
	it "is not valid without body" do
    	@comment = Comment.new :body => nil, :post_id => 1
    	@comment.should_not be_valid
  	end

end
