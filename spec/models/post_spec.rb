require 'spec_helper'

describe Post do
	before(:each) do
		@post = Post.new(:title => "foo", 
      :body => "bar"
    )
	end

  it "is valid with valid attributes" do
    @post.should be_valid
  end
	it "is not valid without a title" do
   	@post.title = nil
		@post.should_not be_valid
  end
	it "is not valid without body" do
    @post = Post.new :body => nil, :title => "foo"
    @post.should_not be_valid
  end

end
